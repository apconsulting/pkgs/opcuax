package opcuax

import (
	"context"
	"errors"
	"log"
	"testing"

	"github.com/awcullen/opcua/client"
)

func write[T any](t *testing.T, value *T, endpoint string, defaultNS string) {
	ctx := context.Background()
	client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := client.Close(ctx)
		if err != nil {
			client.Abort(ctx)
			t.Fatal(err)
		}
	}()

	nss, err := GetNamespaceURIs(client)
	if err != nil {
		t.Fatal(err)
	}

	var obj Obj[T]
	if defaultNS == "" {
		obj, err = NewObj[T](nss)
	} else {
		obj, err = NewObjWithDefaultNS[T](nss, defaultNS)
	}
	if err != nil {
		t.Fatal(err)
	}

	obj.Set(value)

	err = obj.Store(context.TODO(), client)
	if err != nil {
		if !errors.Is(err, ErrNodeWriteValue) {
			t.Fatalf("Store failed: %s", err)
		}
		log.Println(err)
	}
}

func TestWriteObj(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	value := &TestProsysObj{
		ObjBoolVar:   false,
		ObjStringVar: "obj string var",
		ObjProp1:     123.45,
	}
	write[TestProsysObj](t, value, connStr, "")
	read[TestProsysObj](t, connStr, "")
}

func TestWriteProsys(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	value := &TestProsys{
		CodOdt:         "Codice OdT nuovo",
		N_Pezzi_Finiti: 321,
		Stato_Taglio:   3,
		Counter:        111.22,
		Random:         3.14,
		SubObj: TestProsysObj{
			ObjBoolVar:   false,
			ObjStringVar: "obj string var",
			ObjProp1:     123.45,
		},
	}
	write[TestProsys](t, value, connStr, "")
	read[TestProsys](t, connStr, "")
}
