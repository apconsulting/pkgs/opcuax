module gitlab.com/apconsulting/pkgs/opcuax

go 1.21

require (
	github.com/awcullen/opcua v1.2.3
	golang.org/x/exp v0.0.0-20231127185646-65229373498e
)

require (
	github.com/djherbis/buffer v1.2.0 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
