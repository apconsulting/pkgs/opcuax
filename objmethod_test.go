package opcuax

import (
	"context"
	"log"
	"testing"

	"github.com/awcullen/opcua/client"
)

type MyMethodArgs struct {
	Operation string
	Parameter float64
}

type MyMethodResult struct {
	Result float64
}

func TestCall(t *testing.T) {
	endpoint := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	ctx := context.Background()
	client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := client.Close(ctx)
		if err != nil {
			client.Abort(ctx)
			t.Fatal(err)
		}
	}()
	method := Method[MyMethodArgs, MyMethodResult]{}
	method.Init("ns=6;s=MyDevice", "ns=6;s=MyMethod")
	args := MyMethodArgs{
		Operation: "pow",
		Parameter: 2.0,
	}
	result, err := method.Call(client, context.TODO(), args)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(result)
}

func TestFunction(t *testing.T) {
	endpoint := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	ctx := context.Background()
	client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := client.Close(ctx)
		if err != nil {
			client.Abort(ctx)
			t.Fatal(err)
		}
	}()
	foo := NewFunction[MyMethodArgs, MyMethodResult](client, "ns=6;s=MyDevice", "ns=6;s=MyMethod")
	args := MyMethodArgs{
		Operation: "pow",
		Parameter: 2.0,
	}
	result, err := foo(context.TODO(), args)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(result)
}
