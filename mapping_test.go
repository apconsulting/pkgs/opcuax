package opcuax

import (
	"testing"

	"golang.org/x/exp/maps"
)

type TestStructBonetti struct {
	CodOdt       string "opcua:\"nsu=http://Interfaccia server_apconsulting;i=10\""
	DBW8         int16  `opcua:"nsu=http://Interfaccia server_apconsulting;i=7"`
	Stato_Taglio int32  `opcua:"nsu=http://Interfaccia server_apconsulting;i=34"`
	//MyMethod Method[Args, Results] `opcua-ob:"ns=#;i=10"` `opcua-fn:"ns=#;i=10"`
}

type TestStructFaedoLettura struct {
	Stato3421       uint16 `opcua:"s=Local HMI.Tags.lettura.Stato 3421"`
	PosCarrello3421 uint16 `opcua:"s=Local HMI.Tags.lettura.posizione carrello matricola 3421"`
}

func TestStructNodeIDs(t *testing.T) {
	nss := []string{"http://opcfoundation.org/UA/", "http://Interfaccia server_apconsulting", "weintek_statistics"}

	s1 := TestStructBonetti{}
	fieldIDs1, err := structNodeIDs(s1, "", nss)
	if err != nil {
		t.Error(err)
	}
	if NodeIDStr(fieldIDs1.fieldToNodeID["CodOdt"]) != "ns=1;i=10" {
		t.Errorf(`TestStructBonetti.CodOdt should map to "ns=1;i=10", got %s`, NodeIDStr(fieldIDs1.fieldToNodeID["CodOdt"]))
	}
	if NodeIDStr(fieldIDs1.fieldToNodeID["DBW8"]) != "ns=1;i=7" {
		t.Errorf(`TestStructBonetti.DBW8 should map to "ns=1;i=7", got %s`, NodeIDStr(fieldIDs1.fieldToNodeID["DBW8"]))
	}
	if NodeIDStr(fieldIDs1.fieldToNodeID["Stato_Taglio"]) != "ns=1;i=34" {
		t.Errorf(`TestStructBonetti.Stato_Taglio should map to "ns=1;i=34", got %s`, NodeIDStr(fieldIDs1.fieldToNodeID["Stato_Taglio"]))
	}

	t.Log(fieldIDs1.fieldToNodeID)

	s2 := &TestStructFaedoLettura{}
	fieldIDs2, err := structNodeIDs(s2, "weintek_statistics", nss)
	if err != nil {
		t.Error(err)
	}
	if NodeIDStr(fieldIDs2.fieldToNodeID["Stato3421"]) != "ns=2;s=Local HMI.Tags.lettura.Stato 3421" {
		t.Errorf(`TestStructBonetti.Stato3421 should map to "ns=2;s=Local HMI.Tags.lettura.Stato 3421", got "%s"`, NodeIDStr(fieldIDs2.fieldToNodeID["Stato3421"]))
	}
	if NodeIDStr(fieldIDs2.fieldToNodeID["PosCarrello3421"]) != "ns=2;s=Local HMI.Tags.lettura.posizione carrello matricola 3421" {
		t.Errorf(`TestStructBonetti.PosCarrello3421 should map to "ns=2;s=Local HMI.Tags.lettura.posizione carrello matricola 3421", got "%s"`, NodeIDStr(fieldIDs2.fieldToNodeID["PosCarrello3421"]))
	}

	t.Log(fieldIDs2.fieldToNodeID)

	fIDs1, ok := structTypeToFieldNodeIDsCache["opcua.TestStructBonetti"]
	if !ok {
		t.Errorf("opcua.TestStructBonetti should be in structTypeToFieldNodeIDsCache")
	}
	if !maps.Equal(fieldIDs1.fieldToNodeID, fIDs1.fieldToNodeID) {
		t.Errorf("TestStructBonetti nodeIDs should be the same found in structTypeToFieldNodeIDsCache[\"opcua.TestStructBonetti\"]")
	}

	fIDs2, ok := structTypeToFieldNodeIDsCache["opcua.TestStructFaedoLettura"]
	if !ok {
		t.Errorf("opcua.TestStructFaedoLettura should be in structTypeToFieldNodeIDsCache")
	}
	if !maps.Equal(fieldIDs2.fieldToNodeID, fIDs2.fieldToNodeID) {
		t.Errorf("TestStructFaedoLettura nodeIDs should be the same found in structTypeToFieldNodeIDsCache[\"opcua.TestStructFaedoLettura\"]")
	}

	t.Log(structTypeToFieldNodeIDsCache)
}
