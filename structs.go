package opcuax

import (
	"reflect"
	"strings"

	"github.com/awcullen/opcua/ua"
)

func SetVariantField[StructType any](s *StructType, fieldPath string, v ua.Variant) {
	sv := reflect.ValueOf(s).Elem()
	var fv reflect.Value

	parts := strings.Split(fieldPath, ".")
	n := len(parts)
	i := 0
	for i < n {
		fv = sv.FieldByName(parts[i])
		sv = fv
		i++
	}

	vValue := reflect.ValueOf(v)

	if vValue.CanConvert(fv.Type()) {
		vConverted := vValue.Convert(fv.Type())
		fv.Set(vConverted)
	}
}

func SetFieldDataValue[StructType any](s *StructType, n string, v ua.DataValue) {
	SetVariantField(s, n, v.Value)
}

func FieldDataValue[StructType any](s *StructType, fieldPath string) ua.DataValue {
	sv := reflect.ValueOf(s).Elem()
	var fv reflect.Value

	parts := strings.Split(fieldPath, ".")
	n := len(parts)
	i := 0
	for i < n {
		fv = sv.FieldByName(parts[i])
		sv = fv
		i++
	}

	dv := ua.DataValue{
		Value: fv.Interface(),
	}
	return dv
}

func FieldsToVariant(s any) ([]string, []ua.Variant) {
	v := reflect.ValueOf(s)
	if v.Kind() == reflect.Pointer {
		v = v.Elem()
	}
	if v.Kind() != reflect.Struct {
		panic("s must be a struct or a struct pointer")
	}

	n := v.NumField()
	values := make([]ua.Variant, n)
	names := make([]string, n)
	for i := 0; i < n; i++ {
		field := v.Field(i)
		variant := ua.Variant(field.Interface())
		names[i] = v.Type().Field(i).Name
		values[i] = variant
	}
	return names, values
}

func SetFieldsFromVariant[StructType any](s *StructType, values []ua.Variant) {
	t := reflect.TypeOf(s)
	if t.Kind() == reflect.Pointer {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		panic("s must be a struct or a struct pointer")
	}

	n := min(t.NumField(), len(values))
	for i := 0; i < n; i++ {
		field := t.Field(i)
		SetVariantField(s, field.Name, values[i])
	}
}
