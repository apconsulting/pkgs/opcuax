package opcuax

import (
	"context"
	"log"
	"sync"
	"time"

	"github.com/awcullen/opcua/client"
	"github.com/awcullen/opcua/ua"
	"golang.org/x/exp/maps"
)

var _clientHandleCounter uint32
var _clientHandleMx sync.Mutex

type SubscriptionOptions struct {
	PublishingIntervalMillisec float64
	RequestedLifetimeCount     uint32
	RequestedMaxKeepAliveCount uint32
	MaxNotificationsPerPublish uint32
	PublishingDisabled         bool
	Priority                   uint8
	SamplingIntervalMillisec   float64
}

type Subscription[S any] struct {
	value         *S
	id            uint32
	handles       map[uint32]string
	client        *client.Client
	mx            *sync.RWMutex
	cancel        context.CancelFunc
	firstUpdateCh chan struct{}
	broadcast     *struct {
		ch  chan struct{}
		err error
	}
	opts SubscriptionOptions
}

func (sub Subscription[S]) Unsubscribe() error {
	sub.cancel()
	return nil
}

func (sub Subscription[S]) Value() S {
	<-sub.firstUpdateCh

	sub.mx.RLock()
	v := *sub.value
	sub.mx.RUnlock()
	return v
}

func (sub Subscription[S]) WaitChangedValue() (S, error) {
	<-sub.broadcast.ch
	return sub.Value(), sub.broadcast.err
}

func NewSubscription[S any](ctx context.Context, ch *client.Client, opts SubscriptionOptions) (Subscription[S], error) {
	sub := Subscription[S]{
		value:         new(S),
		client:        ch,
		mx:            &sync.RWMutex{},
		handles:       make(map[uint32]string),
		firstUpdateCh: make(chan struct{}, 1),
		broadcast: &struct {
			ch  chan struct{}
			err error
		}{make(chan struct{}), nil},
		opts: opts,
	}

	// prepare create subscription request
	req := &ua.CreateSubscriptionRequest{
		RequestedPublishingInterval: opts.PublishingIntervalMillisec,
		RequestedMaxKeepAliveCount:  opts.RequestedMaxKeepAliveCount,
		RequestedLifetimeCount:      opts.RequestedLifetimeCount,
		PublishingEnabled:           !opts.PublishingDisabled,
		MaxNotificationsPerPublish:  opts.MaxNotificationsPerPublish,
		Priority:                    opts.Priority,
	}

	// send request to server. receive response or error
	res, err := ch.CreateSubscription(ctx, req)
	if err != nil {
		return sub, err
	}

	sub.id = res.SubscriptionID

	URIs, err := GetNamespaceURIs(ch)
	if err != nil {
		return sub, err
	}
	nodeIDs, err := structNodeIDs(sub.value, "", URIs)
	if err != nil {
		return sub, err
	}

	itemsToCreate := make([]ua.MonitoredItemCreateRequest, len(maps.Values(nodeIDs.fieldToNodeID)))

	i := 0
	for n, v := range nodeIDs.fieldToNodeID {
		_clientHandleMx.Lock()
		clientHandle := _clientHandleCounter + 1
		_clientHandleCounter = clientHandle
		_clientHandleMx.Unlock()
		sub.handles[clientHandle] = n

		itemsToCreate[i] = ua.MonitoredItemCreateRequest{
			ItemToMonitor: ua.ReadValueID{
				NodeID:      v,
				AttributeID: ua.AttributeIDValue,
			},
			MonitoringMode: ua.MonitoringModeReporting,
			RequestedParameters: ua.MonitoringParameters{
				ClientHandle:     clientHandle,
				QueueSize:        1,
				DiscardOldest:    true,
				SamplingInterval: opts.SamplingIntervalMillisec,
			},
		}
		i++
	}

	// prepare create monitored items request
	req2 := &ua.CreateMonitoredItemsRequest{
		SubscriptionID:     res.SubscriptionID,
		TimestampsToReturn: ua.TimestampsToReturnBoth,
		ItemsToCreate:      itemsToCreate,
	}

	// send request to server. receive response or error
	_, err = ch.CreateMonitoredItems(ctx, req2)
	if err != nil {
		return sub, err
	}

	loopCtx, cancel := context.WithCancel(ctx)
	sub.cancel = cancel
	go subscriptionLoop(loopCtx, sub)

	return sub, nil
}

func signal[S any](sub Subscription[S]) {
	close(sub.broadcast.ch)
	sub.broadcast.ch = make(chan struct{})
}

func subscriptionLoop[S any](ctx context.Context, sub Subscription[S]) {
	// prepare an initial publish request
	req := &ua.PublishRequest{
		RequestHeader: ua.RequestHeader{TimeoutHint: 60000},
		SubscriptionAcknowledgements: []ua.SubscriptionAcknowledgement{
			{
				SubscriptionID: sub.id,
			},
		},
	}

	var res *ua.PublishResponse

	firstPublish := true

	for {
		select {
		case <-ctx.Done():
			log.Println("done")
			close(sub.broadcast.ch)
			return
		default:
			log.Println("publish")

			// send publish request to the server.
			res, sub.broadcast.err = sub.client.Publish(ctx, req)
			if sub.broadcast.err != nil {
				log.Println(sub.broadcast.err)
				signal(sub)
				time.Sleep(1 * time.Second)
				break
			}

			//log.Println(res)

			// loop thru all the notifications in the response.
			for _, data := range res.NotificationMessage.NotificationData {
				switch body := data.(type) {
				case ua.DataChangeNotification:
					// the data change notification contains a slice of monitored item notifications.
					sub.mx.Lock()
					for _, item := range body.MonitoredItems {
						// each monitored item notification contains a clientHandle and dataValue.
						if fieldName, ok := sub.handles[item.ClientHandle]; ok {
							SetFieldDataValue[S](sub.value, fieldName, item.Value)
						}
					}
					signal(sub)
					sub.mx.Unlock()
				default:
					log.Println(data)
				}
			}

			if firstPublish {
				sub.firstUpdateCh <- struct{}{}
				firstPublish = false
				close(sub.firstUpdateCh)
			}

			// prepare another publish request
			req = &ua.PublishRequest{
				RequestHeader: ua.RequestHeader{TimeoutHint: 60000},
				SubscriptionAcknowledgements: []ua.SubscriptionAcknowledgement{
					{
						SequenceNumber: res.NotificationMessage.SequenceNumber,
						SubscriptionID: res.SubscriptionID,
					},
				},
			}
		}
	}

}
