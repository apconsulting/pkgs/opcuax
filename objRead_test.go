package opcuax

import (
	"context"
	"log"
	"testing"

	"github.com/awcullen/opcua/client"
)

type TestProsysObj struct {
	ObjBoolVar   bool    `opcua:"ns=3;i=1011"`
	ObjStringVar string  `opcua:"ns=3;i=1012"`
	ObjProp1     float64 `opcua:"ns=3;i=1013"`
}

type TestProsys struct {
	CodOdt         string  "opcua:\"ns=3;i=1007\""
	N_Pezzi_Finiti int32   `opcua:"ns=3;i=1008"`
	Stato_Taglio   int32   `opcua:"ns=3;i=1009"`
	Counter        float64 `opcua:"ns=3;i=1001"`
	Random         float64 `opcua:"ns=3;i=1002"`
	SubObj         TestProsysObj
}

func read[T any](t *testing.T, endpoint string, defaultNS string) {
	ctx := context.Background()
	client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := client.Close(ctx)
		if err != nil {
			client.Abort(ctx)
			t.Fatal(err)
		}
	}()

	nss, err := GetNamespaceURIs(client)
	if err != nil {
		t.Fatal(err)
	}

	var obj Obj[T]
	if defaultNS == "" {
		obj, err = NewObj[T](nss)
	} else {
		obj, err = NewObjWithDefaultNS[T](nss, defaultNS)
	}
	if err != nil {
		t.Fatal(err)
	}

	err = obj.Load(context.TODO(), client)
	if err != nil {
		t.Fatalf("Read failed: %s", err)
	}

	log.Println(obj.Get())
}

func TestReadObj(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	read[TestProsysObj](t, connStr, "")
}

func TestReadProsys(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	read[TestProsys](t, connStr, "")
}

type TestFaedoLettura struct {
	Stato3421       uint16 `opcua:"s=Local HMI.Tags.lettura.Stato 3421"`
	PosCarrello3421 uint32 `opcua:"s=Local HMI.Tags.lettura.posizione carrello matricola 3421"`
}

func TestRead2(t *testing.T) {
	connStr := "opc.tcp://192.168.3.21:4840/"
	read[TestFaedoLettura](t, connStr, "weintek_statistics")
}
