package opcuax

import (
	"context"
	"errors"
	"fmt"

	"github.com/awcullen/opcua/client"
	"github.com/awcullen/opcua/ua"

	"golang.org/x/exp/maps"
)

type Obj[T any] struct {
	value   *T
	nodeIDs StructNodeIDSet
}

func NewObj[T any](nsURIs []string) (Obj[T], error) {
	return NewObjWithDefaultNS[T](nsURIs, "")
}

var _namespaceURIs []string
var _serverURIs []string

func requestNSAndSrvURIs(client *client.Client) error {
	var readRequest = &ua.ReadRequest{
		NodesToRead: []ua.ReadValueID{
			{
				NodeID:      ua.VariableIDServerNamespaceArray,
				AttributeID: ua.AttributeIDValue,
			},
			{
				NodeID:      ua.VariableIDServerServerArray,
				AttributeID: ua.AttributeIDValue,
			},
		},
	}
	readResponse, err := client.Read(context.TODO(), readRequest)
	if err != nil {
		return err
	}
	if len(readResponse.Results) == 2 {
		if readResponse.Results[0].StatusCode.IsGood() {
			_namespaceURIs = readResponse.Results[0].Value.([]string)
		}

		if readResponse.Results[1].StatusCode.IsGood() {
			_serverURIs = readResponse.Results[1].Value.([]string)
		}
	}

	return nil
}

func GetNamespaceURIs(client *client.Client) ([]string, error) {
	if _namespaceURIs == nil {
		err := requestNSAndSrvURIs(client)
		if err != nil {
			return nil, err
		}
	}
	return _namespaceURIs, nil
}

func GetServerURIs(client *client.Client) ([]string, error) {
	if _serverURIs == nil {
		err := requestNSAndSrvURIs(client)
		if err != nil {
			return nil, err
		}
	}
	return _serverURIs, nil
}

func NewObjWithDefaultNS[T any](URIs []string, defaultNS string) (Obj[T], error) {
	obj := Obj[T]{}

	value := new(T)
	nodeIDs, err := structNodeIDs(value, defaultNS, URIs)
	if err != nil {
		return obj, err
	}

	obj.value = value
	obj.nodeIDs = nodeIDs
	return obj, nil
}

func (obj Obj[T]) Get() *T {
	return obj.value
}

func (obj *Obj[T]) Set(v *T) {
	obj.value = v
}

var ErrOpcua = errors.New("opcua error")
var ErrNodeReadValue = errors.New("error reading value of node")
var ErrNodeWriteValue = errors.New("error writing value of node")
var ErrWrongNumberOfNames = errors.New("there must be as many field names as results")

func responseError(response any, fieldNames []string) (err error) {
	errs := make([]error, 0)
	switch resp := response.(type) {
	case *ua.ReadResponse:
		if len(fieldNames) != len(resp.Results) {
			return ErrWrongNumberOfNames
		}
		for i, result := range resp.Results {
			if !result.StatusCode.IsGood() {
				name := fieldNames[i]
				e := fmt.Errorf("%w %s: %w", ErrNodeReadValue, name, result.StatusCode)
				errs = append(errs, e)
			}
		}
	case *ua.WriteResponse:
		if len(fieldNames) != len(resp.Results) {
			return ErrWrongNumberOfNames
		}
		for i, status := range resp.Results {
			if !status.IsGood() {
				name := fieldNames[i]
				e := fmt.Errorf("%w %s: %w", ErrNodeWriteValue, name, status)
				errs = append(errs, e)
			}
		}
	default:
		panic("uknown response type")
	}
	return errors.Join(errs...)
}

func (obj Obj[T]) Load(ctx context.Context, client *client.Client) error {
	req := &ua.ReadRequest{
		MaxAge:             2000, // ??? Che inplicazioni ha esattamente?
		TimestampsToReturn: ua.TimestampsToReturnBoth,
		NodesToRead:        make([]ua.ReadValueID, len(maps.Values(obj.nodeIDs.fieldToNodeID))),
	}

	i := 0
	fieldNames := make([]string, len(obj.nodeIDs.fieldToNodeID))
	for n, v := range obj.nodeIDs.fieldToNodeID {
		req.NodesToRead[i] = ua.ReadValueID{NodeID: v, AttributeID: ua.AttributeIDValue}
		fieldNames[i] = n
		i++
	}

	resp, err := client.Read(ctx, req)
	if err != nil {
		return err
	}

	for i, result := range resp.Results {
		if !result.StatusCode.IsGood() {
			continue
		}

		nodeID := req.NodesToRead[i].NodeID
		fieldPath := obj.nodeIDs.nodeIDToField[NodeIDStr(nodeID)]
		SetFieldDataValue(obj.value, fieldPath, result)
	}

	return responseError(resp, fieldNames)
}

func (obj Obj[T]) Store(ctx context.Context, client *client.Client) error {
	req := &ua.WriteRequest{
		NodesToWrite: make([]ua.WriteValue, len(maps.Values(obj.nodeIDs.fieldToNodeID))),
	}

	i := 0
	fieldNames := make([]string, len(obj.nodeIDs.fieldToNodeID))
	for fn, nid := range obj.nodeIDs.fieldToNodeID {
		req.NodesToWrite[i] = ua.WriteValue{
			NodeID:      nid,
			Value:       FieldDataValue(obj.value, fn),
			AttributeID: ua.AttributeIDValue,
		}
		fieldNames[i] = fn
		i++
	}

	resp, err := client.Write(ctx, req)
	if err != nil {
		return err
	}

	return responseError(resp, fieldNames)
}
