package opcuax

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/awcullen/opcua/ua"
)

const tagName = "opcua"

func NodeIDStr(nid ua.NodeID) string {
	switch n := nid.(type) {
	case ua.NodeIDNumeric:
		return n.String()
	case ua.NodeIDString:
		return n.String()
	case ua.NodeIDGUID:
		return n.String()
	case ua.NodeIDOpaque:
		return n.String()
	}
	return ""
}

type StructNodeIDSet struct {
	fieldToNodeID map[string]ua.NodeID
	nodeIDToField map[string]string
}

func NewStructNodeIDSet() StructNodeIDSet {
	set := StructNodeIDSet{
		fieldToNodeID: make(map[string]ua.NodeID),
		nodeIDToField: make(map[string]string),
	}
	return set
}

func (set StructNodeIDSet) AddField(fieldName string, nodeID ua.NodeID) {
	set.fieldToNodeID[fieldName] = nodeID
	set.nodeIDToField[NodeIDStr(nodeID)] = fieldName
}

func (set StructNodeIDSet) AddStructField(fieldName string, subSet StructNodeIDSet) {
	for k, v := range subSet.fieldToNodeID {
		set.fieldToNodeID[k] = v
	}
	for k, v := range subSet.nodeIDToField {
		set.nodeIDToField[k] = v
	}
}

type structTypeToFieldNodeIDs map[string]StructNodeIDSet

var structTypeToFieldNodeIDsCache = make(structTypeToFieldNodeIDs, 4)

func parseOpcuaTag(tag, defaultNS string, nss []string) (ua.NodeID, error) {
	p := strings.SplitN(tag, ";", 2)
	if len(p) == 1 {
		tag = fmt.Sprintf("nsu=%s;%s", defaultNS, p[0])
	}
	exNodeID := ua.ParseExpandedNodeID(tag)
	return ua.ToNodeID(exNodeID, nss), nil
}

func structTypeNodeIDs(t reflect.Type, subPrefix, defaultNS string, nss []string) (StructNodeIDSet, error) {
	set := NewStructNodeIDSet()

	n := t.NumField()
	for i := 0; i < n; i++ {
		field := t.Field(i)

		if !field.IsExported() {
			continue
		}

		name := field.Name
		if subPrefix != "" {
			name = subPrefix + "." + name
		}

		if field.Type.Kind() == reflect.Struct {
			subSet, err := structTypeNodeIDs(field.Type, name, defaultNS, nss)
			if err != nil {
				return subSet, err
			}
			set.AddStructField(name, subSet)
			continue
		}

		tag := field.Tag.Get(tagName)
		if len(tag) == 0 {
			continue
		}

		nodeID, err := parseOpcuaTag(tag, defaultNS, nss)
		if err != nil {
			return set, err
		}
		set.AddField(name, nodeID)
	}

	return set, nil
}

func structNodeIDs(s any, defaultNS string, nss []string) (StructNodeIDSet, error) {
	t := reflect.TypeOf(s)
	if t.Kind() == reflect.Pointer {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		return StructNodeIDSet{}, fmt.Errorf("argument must be a struct or a pointer to struct")
	}

	if cachedSet, ok := structTypeToFieldNodeIDsCache[t.String()]; ok {
		return cachedSet, nil
	}

	set, err := structTypeNodeIDs(t, "", defaultNS, nss)
	if err != nil {
		return set, err
	}

	structTypeToFieldNodeIDsCache[t.String()] = set

	return set, nil
}
