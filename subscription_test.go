package opcuax

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/awcullen/opcua/client"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

type TestProsysObjSub struct {
	ObjBoolVar   bool    `opcua:"ns=3;i=1011"`
	ObjStringVar string  `opcua:"ns=3;i=1012"`
	ObjProp1     float64 `opcua:"ns=3;i=1013"`
}

type TestProsysSub struct {
	CodOdt         string  "opcua:\"ns=3;i=1007\""
	N_Pezzi_Finiti int32   `opcua:"ns=3;i=1008"`
	Stato_Taglio   int32   `opcua:"ns=3;i=1009"`
	Counter        float64 `opcua:"ns=3;i=1001"`
	Random         float64 `opcua:"ns=3;i=1002"`
	SubObj         TestProsysObj
}

var subOpts = SubscriptionOptions{
	PublishingIntervalMillisec: 1000,
	RequestedMaxKeepAliveCount: 30,
	RequestedLifetimeCount:     30 * 3,
	SamplingIntervalMillisec:   500,
}

func subscribe[T any](t *testing.T, endpoint string) Subscription[T] {
	ctx := context.Background()
	client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
	if err != nil {
		t.Fatal(err)
	}

	sub, err := NewSubscription[T](ctx, client, subOpts)
	if err != nil {
		t.Fatal(err)
	}

	return sub
}

func TestSubscriptionValue(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	sub := subscribe[TestProsysObj](t, connStr)
	defer func() {
		ctx := context.Background()
		err := sub.client.Close(ctx)
		if err != nil {
			sub.client.Abort(ctx)
			t.Fatal("close error, aborting...", err)
		}
	}()
	go func() {
		time.Sleep(1500 * time.Millisecond)
		ctx := context.Background()
		nss, err := GetNamespaceURIs(sub.client)
		if err != nil {
			log.Fatalln(err)
		}
		obj, err := NewObj[TestProsysObj](nss)
		if err != nil {
			log.Fatalln(err)
		}
		value := &TestProsysObj{
			ObjBoolVar:   true,
			ObjStringVar: "subscription test string var",
			ObjProp1:     54.321,
		}
		obj.Set(value)
		err = obj.Store(ctx, sub.client)
		if err != nil {
			log.Fatalln(err)
		}
	}()
	for i := 0; i < 10; i++ {
		v := sub.Value()
		log.Println(v)
		time.Sleep(1500 * time.Millisecond)
	}
	sub.Unsubscribe()
	time.Sleep(1 * time.Second)
}

func TestSubscriptionWaitChangedValue(t *testing.T) {
	connStr := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	sub := subscribe[TestProsysObj](t, connStr)
	defer func() {
		ctx := context.Background()
		err := sub.client.Close(ctx)
		if err != nil {
			sub.client.Abort(ctx)
			t.Fatal("close error, aborting...", err)
		}
	}()
	go func() {
		ctx := context.Background()
		nss, err := GetNamespaceURIs(sub.client)
		if err != nil {
			log.Fatalln(err)
		}
		obj, err := NewObj[TestProsysObj](nss)
		if err != nil {
			log.Fatalln(err)
		}
		for i := 0; i < 3; i++ {
			time.Sleep(1500 * time.Millisecond)
			value := &TestProsysObj{
				ObjBoolVar:   i%2 == 0,
				ObjStringVar: "subscription test string var",
				ObjProp1:     54.321 + float64(i),
			}
			obj.Set(value)
			err = obj.Store(ctx, sub.client)
			if err != nil {
				log.Fatalln(err)
			}
		}
		time.Sleep(500 * time.Millisecond)
		sub.Unsubscribe()
	}()
	for i := 0; i < 10; i++ {
		v, err := sub.WaitChangedValue()
		if err != nil {
			log.Println(err)
		}
		log.Println(v)
	}
}

func TestSubscriptionDisconnect(t *testing.T) {
	endpoint := "opc.tcp://127.0.0.1:53530/OPCUA/SimulationServer"
	const MaxWaitingSecs = 1024
	waitForSecs := int64(1)
	for {
		ctx := context.Background()
		client, err := client.Dial(ctx, endpoint, client.WithInsecureSkipVerify())
		if err != nil {
			log.Println(err)
			time.Sleep(time.Duration(waitForSecs) * time.Second)
			waitForSecs = min(waitForSecs<<1, MaxWaitingSecs)
			continue
		}
		waitForSecs = 1

		sub, err := NewSubscription[TestProsysObj](ctx, client, subOpts)
		if err != nil {
			t.Fatal(err)
		}
		// go func() {
		// 	ctx := context.Background()
		// 	nss, err := GetNamespaceURIs(sub.client)
		// 	if err != nil {
		// 		log.Fatalln(err)
		// 	}
		// 	obj, err := NewObj[TestProsysObj](nss)
		// 	if err != nil {
		// 		log.Fatalln(err)
		// 	}
		// 	for i := 0; i < 30; i++ {
		// 		time.Sleep(1500 * time.Millisecond)
		// 		value := &TestProsysObj{
		// 			ObjBoolVar:   i%2 == 0,
		// 			ObjStringVar: "subscription test string var",
		// 			ObjProp1:     54.321 + float64(i),
		// 		}
		// 		obj.Set(value)
		// 		err = obj.Store(ctx, sub.client)
		// 		if err != nil {
		// 			log.Fatalln(err)
		// 		}
		// 	}
		// 	time.Sleep(500 * time.Millisecond)
		// }()
		for i := 0; i < 100; i++ {
			v, err := sub.WaitChangedValue()
			if err != nil {
				log.Println(err)
				sub.Unsubscribe()
				ctx := context.Background()
				closeErr := client.Close(ctx)
				if closeErr != nil {
					client.Abort(ctx)
				}
				break
			}
			log.Println(v)
		}
	}
}
