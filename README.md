OPCUA eXperiment
----------------

An experimental package that tries to simplify the impossibly complex OPC-UA protocol.
It is based on github.com/awcullen/opcua, a native go implementation of protocol specification.