package opcuax

import (
	"context"
	"fmt"

	"github.com/awcullen/opcua/client"
	"github.com/awcullen/opcua/ua"
)

// Method represents a callable object method.
// NB: ArgStruct and ResultStruct must be struct types
type Method[ArgStruct, ResultStruct any] struct {
	obj ua.ExpandedNodeID
	fun ua.ExpandedNodeID
}

func (method *Method[ArgStruct, ResultStruct]) Init(objID, funID string) {
	method.obj = ua.ParseExpandedNodeID(objID)
	method.fun = ua.ParseExpandedNodeID(funID)
}

func (method *Method[ArgStruct, ResultStruct]) Call(client *client.Client, ctx context.Context, args ArgStruct) (ResultStruct, error) {
	var result ResultStruct
	nss, err := GetNamespaceURIs(client)
	if err != nil {
		return result, err
	}

	argNames, methodArgs := FieldsToVariant(args)
	req := &ua.CallRequest{
		MethodsToCall: []ua.CallMethodRequest{
			{
				ObjectID:       ua.ToNodeID(method.obj, nss),
				MethodID:       ua.ToNodeID(method.fun, nss),
				InputArguments: methodArgs,
			},
		},
	}
	response, err := client.Call(ctx, req)
	if err != nil {
		return result, err
	}
	if len(response.Results) != 1 {
		return result, fmt.Errorf("unexpected number of results")
	}
	for i, status := range response.Results[0].InputArgumentResults {
		if !status.IsGood() {
			return result, fmt.Errorf("invalid input argument \"%s\": %w", argNames[i], status)
		}
	}
	SetFieldsFromVariant(&result, response.Results[0].OutputArguments)
	return result, nil
}

type Function[ArgStruct, ResultStruct any] func(context.Context, ArgStruct) (ResultStruct, error)

func NewFunction[ArgStruct, ResultStruct any](client *client.Client, objID, funID string) Function[ArgStruct, ResultStruct] {
	method := Method[ArgStruct, ResultStruct]{}
	method.Init(objID, funID)
	return func(ctx context.Context, args ArgStruct) (ResultStruct, error) {
		return method.Call(client, ctx, args)
	}
}
